#!/bin/bash

# Script to create a Mumble release tarball from Git including codec
# submodules.  This is meant to be run from within a directory
# containing a Mumble Git repository from Github, and after running
# 'git submodule init' and 'git submodule update' within the directory.
# The resulting tarball will appear one directory up from the repo.
#
# Author: Christopher Knadle <Chris.Knadle@coredump.us>
# Initial Date: 2018-12-24
# Last-Updated: 2019-01-13

# Change this location to point to the included patch
MUMBLE_INI_PATCH_LOCATION=../murmur.ini.system.diff

set -e

# Get Mumble major.minor.patch version
VERSION=$(grep "^VERSION" src/mumble.pri | cut -d '=' -f 2 | tr -d ' ' | tr -d '\t')
# Choose mumble tarball prefix (and part of tarball filename) based on
# Get ISO date of latest Git commit (in 'master') and Git commit #
GitCommitVersion=$(git log master | head -n 1 | cut -b 41-)
GitCommitDate=$(git log --date=iso master | head -n 4 | tail -n 1 | cut -b 9-18 | tr -d '-')
GitVersionPrefix="$VERSION~git$GitCommitDate.$GitCommitVersion"
echo "Combined version will be: $GitVersionPrefix+dfsg"

# Save working directory to come back to from tarring submodules
WORKDIR=$(pwd)

# Create Git archives of submodules first
cat .gitmodules | fgrep path | \
while read junk junk subpath
do
   #subpath_filename=$(echo "$subpath" | tr '/' '#')
   subpath_basename=$(basename "$subpath")
   echo "making tarball of $subpath_basename"
   cd $subpath
   # If the submodule has a 'master' branch, use it, otherwise use HEAD
   mastercheck=$(git branch --list master)
   if [ -n "$mastercheck" ]; then
      BRANCH="master"
   else
      BRANCH="HEAD"
   fi
   echo "Using '$BRANCH' for $subpath repo"
   git archive --prefix=$GitVersionPrefix/$subpath/ --format tar $BRANCH --output $WORKDIR/../$subpath_basename.submodule.tar
   cd $WORKDIR
done

# Create Git archive of main repository
echo "Making root tarball"
git archive --prefix=$GitVersionPrefix/ --format tar master --output $WORKDIR/../maindir.tar

# Concatenate tarballs into one big tarball
echo "Combining submodule tarballs."
touch ../$GitVersionPrefix.tar
cat .gitmodules | fgrep path | \
while read junk junk subpath
do
   subpath_basename=$(basename "$subpath")
   echo "Concatenating $subpath_basename tarball"
   tar --concatenate --file ../$GitVersionPrefix.tar $WORKDIR/../$subpath_basename.submodule.tar
done
echo "Concatenating root tarball"
tar --concatenate --file ../$GitVersionPrefix.tar ../maindir.tar

# Cleaning up submodule and maindir tarballs
rm -v ../*.submodule.tar
rm -v ../maindir.tar

# Expand tarball to prep to remove files not meant to be released
echo "Expanding tarball to remove unnecessary files."
cd $WORKDIR/..
tar -xf $GitVersionPrefix.tar
rm $GitVersionPrefix.tar
cd $GitVersionPrefix
rm -rf .tx/
rm .mailmap
rm .travis.yml
#rm 3rdparty/opus-src/update_version
find . -name .gitignore  -exec rm '{}' \;
find . -name .gitmodules -exec rm '{}' \;

# Create scripts/murmur.ini.system and patch for release
echo "Creating scripts/murmur.ini.system"
cp -a scripts/murmur.ini scripts/murmur.ini.system
patch -p0 < $MUMBLE_INI_PATCH_LOCATION

# Remove unreleasable files (IETF draft documentation with non-free licenses)
echo "Removing non-free documentation directories"
rm -rf 3rdparty/celt-0.11.0-src/doc/
rm -rf 3rdparty/celt-0.7.0-src/doc/
rm -rf 3rdparty/opus-src/doc/
rm -rf 3rdparty/speex-src/doc/
rm -rf 3rdparty/speexdsp-src/doc
# Remove empty directory as Lintian complains it blocks tarball entry into Git
rmdir  3rdparty/sbcelt-src/celt-0.7.0/
# Create softlinks to codecs
echo "Creating codec softlinks"
ln -s 3rdparty/celt-0.11.0-src/ celt-0.11.0-src
ln -s 3rdparty/celt-0.7.0-src/  celt-0.7.0-src
ln -s 3rdparty/opus-src/        opus-src
ln -s 3rdparty/sbcelt-src/      sbcelt-src
ln -s 3rdparty/speex-src/       speex

# Repack tarball into .tar.gz
echo "Repacking tarball into .tar.gz"
cd ..
tar -czf $GitVersionPrefix+dfsg.tar.gz $GitVersionPrefix
echo "Created ../$GitVersionPrefix+dfsg.tar.gz"

# Clean up expanded files
rm -rf $GitVersionPrefix

# Leave off in the same directory where script was started
cd $WORKDIR
