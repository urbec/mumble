mumble (1.3.0~2868~g44b9004+dfsg-1) experimental; urgency=medium

    This is the first version of Mumble in Debian that is built with Qt5
    and thus has SSL support for ciphers that support PFS (Perfect Forward
    Secrecy).  The package has been tested to insure that the basic
    functionality of the client, server, and overlay all function.

    There are currently unreleasable files in the upstream tarball that are
    part of Git submodules (draft IETF documents for codecs that have a
    restrictive license) so the normal PGP check against the upstream tarball
    will not succeed and thus the PGP key has been removed and the uscan check
    disabled for now.

 -- Christopher Knadle <Chris.Knadle@coredump.us>  Wed, 20 Sep 2018 02:55:52 +0000

mumble (1.2.19-1) unstable; urgency=medium

    Qt 4 is to be removed from the Debian archive before the release of
    Buster (because it is out of support upstream) which is a dependency for
    Mumble 1.2.x, so Mumble will be soon be removed with it.  Mumble 1.2.x
    will not build with Qt 5. Mumble 1.3.x can build with Qt 5 but does not
    yet have a stable release that could be uploaded to Unstable.
    Work is proceeding to upload a Mumble 1.3.x snapshot to Experimental.
    To monitor when this occurs, watch Bug #874683.
       https://bugs.debian.org/874683

    OpenSSL 1.0 is also to be removed, and Qt 5 does not work with OpenSSL 1.1
    until Qt 5.10.0 which is not yet released upstream.  At present Qt 4 in
    Unstable also doesn't work with OpenSSL 1.1, but qt4-x11 4:4.8.7+dfsg-13
    which does has been uploaded to Experimental.
    For more information on the OpenSSL 1.0 removal, see:
       https://lists.debian.org/debian-devel-announce/2017/10/msg00001.html

 -- Christopher Knadle <Chris.Knadle@coredump.us>  Tue, 05 Dec 2017 21:57:46 +0000

mumble (1.2.0~200910141302-8af721-1) experimental; urgency=low

    The 1.2.0 series uses a new protocol, which makes it incompatible with 1.1.8
    and earlier version. If you wish to connect to an older server, there is a
    separate mumble-11x package with a backward compatible client (mumble11x).
    There is no backward-compatible server, so if you upgrade the server connecting
    clients must be running 1.2.0 or newer.

 -- Thorvald Natvig <slicer@users.sourceforge.net>  Fri, 02 Oct 2009 15:35:54 +0200
